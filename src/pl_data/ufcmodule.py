from typing import Optional, Sequence
import hydra
import torch
import random
import omegaconf
import numpy as np
import pytorch_lightning as pl

from torch.utils.data import DataLoader
from torchvision.datasets import Kinetics
from omegaconf import DictConfig, ValueNode
from torchvision.transforms import ConvertImageDtype, Compose, Resize, Normalize

from src.common.utils import PROJECT_ROOT

def worker_init_fn(id: int):
    """
    DataLoaders workers init function.

    Initialize the numpy.random seed correctly for each worker, so that
    random augmentations between workers and/or epochs are not identical.

    If a global seed is set, the augmentations are deterministic.

    https://pytorch.org/docs/stable/notes/randomness.html#dataloader
    """
    uint64_seed = torch.initial_seed()
    ss = np.random.SeedSequence([uint64_seed])
    # More than 128 bits (4 32-bit words) would be overkill.
    np.random.seed(ss.generate_state(4))
    random.seed(uint64_seed)


class UFCDataModule(pl.LightningDataModule):
    def __init__(
        self,
        dataset: DictConfig,
        img_size:ValueNode,
        num_workers: DictConfig,
        batch_size: DictConfig,
    ):
        super().__init__()
        self.dataset = dataset
        self.num_workers = num_workers
        self.batch_size = batch_size
        self.img_size = img_size

        self.train_dataset: Optional[Kinetics] = None
        self.val_datasets: Optional[Sequence[Kinetics]] = None
        self.test_datasets: Optional[Sequence[Kinetics]] = None

    def prepare_data(self) -> None:
        # download only
        pass

    def setup(self, stage: Optional[str] = None):

        transf = Compose([Resize((self.img_size, self.img_size),antialias=True), ConvertImageDtype(torch.float),
        Normalize([0.485, 0.456, 0.406],[0.229, 0.224, 0.225])])

        if stage is None or stage == "fit":
            self.train_dataset = hydra.utils.instantiate(self.dataset,transform= transf, split="train")
            self.val_datasets = [
                hydra.utils.instantiate(self.dataset,transform= transf, split="val")
            ]

        if stage is None or stage == "test":
            self.test_datasets = [
                hydra.utils.instantiate(self.dataset,transform= transf, split="test")
            ]
        
    def train_dataloader(self) -> DataLoader:
        return DataLoader(
            self.train_dataset,
            shuffle=True,
            batch_size=self.batch_size.train,
            num_workers=self.num_workers.train,
            worker_init_fn=worker_init_fn,
            prefetch_factor=4,
            pin_memory=True,
        )

    def val_dataloader(self) -> Sequence[DataLoader]:
        return [DataLoader(
                self.val_datasets[0],
                batch_size= self.batch_size.val,
                num_workers=self.num_workers.val,
                worker_init_fn=worker_init_fn,
            )]

    def test_dataloader(self) -> Sequence[DataLoader]:
        return [
            DataLoader(
                self.test_datasets[0],
                batch_size=self.batch_size.test,
                num_workers=self.num_workers.test,
                worker_init_fn=worker_init_fn,
            )
        ]

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}("
            f"self.dataset = {self.dataset}, "
            f"self.num_workers = {self.num_workers}, "
            f"self.batch_size = {self.batch_size})"
        )

def convertDtype(dtype: str):
    return ConvertImageDtype(eval(dtype)) 

@hydra.main(config_path=str(PROJECT_ROOT / "conf"), config_name="default")
def main(cfg: omegaconf.DictConfig):
    datamodule: pl.LightningDataModule = hydra.utils.instantiate(
        cfg.data.datamodule, _recursive_=False
    )
    datamodule.setup()
    train = datamodule.train_dataloader()
    first = next(iter(train))
    print(first['video'].shape, first['label'].shape)
    

if __name__ == "__main__":
    main()
