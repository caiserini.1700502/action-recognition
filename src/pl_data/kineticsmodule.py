from multiprocessing import Lock
from typing import Dict, Optional, Sequence
import os
import hydra
import torch
import random
import omegaconf
import numpy as np
import pytorch_lightning as pl

from torchvision import transforms
from torch.utils.data import DataLoader
from torchvision.datasets import Kinetics
from omegaconf import DictConfig, ValueNode
from tqdm import tqdm

from src.common.utils import PROJECT_ROOT

from joblib import Parallel, delayed

def worker_init_fn(id: int):
    """
    DataLoaders workers init function.

    Initialize the numpy.random seed correctly for each worker, so that
    random augmentations between workers and/or epochs are not identical.

    If a global seed is set, the augmentations are deterministic.

    https://pytorch.org/docs/stable/notes/randomness.html#dataloader
    """
    uint64_seed = torch.initial_seed()
    ss = np.random.SeedSequence([uint64_seed])
    # More than 128 bits (4 32-bit words) would be overkill.
    np.random.seed(ss.generate_state(4))
    random.seed(uint64_seed)


class VideoDataModule(pl.LightningDataModule):
    def __init__(
        self,
        dataset: DictConfig,
        img_size:ValueNode,
        num_workers: DictConfig,
        batch_size: DictConfig,
    ):
        super().__init__()
        self.dataset = dataset
        self.num_workers = num_workers
        self.batch_size = batch_size
        self.img_size = img_size

        self.train_dataset: Optional[Kinetics] = None
        self.val_datasets: Optional[Sequence[Kinetics]] = None
        self.test_datasets: Optional[Sequence[Kinetics]] = None

    def prepare_data(self) -> None:
        # download only
        pass

    def setup(self, stage: Optional[str] = None):
        # Here you should instantiate your dataset, you may also split the train into train and validation if needed.
        transf = transforms.Compose([
            transforms.Resize((self.img_size, self.img_size)),
            transforms.Lambda(lambda x: x / 255.),
            #transforms.ToTensor(),
        ])
        if stage is None or stage == "fit":
            self.train_dataset = hydra.utils.instantiate(self.dataset,transform= transf, split="train")
            self.val_datasets = [
                hydra.utils.instantiate(self.dataset,transform= transf, split="val")
                # hydra.utils.instantiate(dataset_cfg)
                # for dataset_cfg in self.datasets.val
            ]

        if stage is None or stage == "test":
            self.test_datasets = [
                hydra.utils.instantiate(self.dataset,transform= transf, split="test")
                # hydra.utils.instantiate(dataset_cfg)
                # for dataset_cfg in self.datasets.test
            ]

    def train_dataloader(self) -> DataLoader:
        return DataLoader(
            self.train_dataset,
            shuffle=True,
            batch_size=self.batch_size.train,
            num_workers=self.num_workers.train,
            worker_init_fn=worker_init_fn,
            prefetch_factor=4,
        )

    def val_dataloader(self) -> Sequence[DataLoader]:
        return [
            DataLoader(
                dataset,
                shuffle=False,
                batch_size=self.batch_size.val,
                num_workers=self.num_workers.val,
                worker_init_fn=worker_init_fn,
            )
            for dataset in self.val_datasets
        ]

    def test_dataloader(self) -> Sequence[DataLoader]:
        return [
            DataLoader(
                dataset,
                shuffle=False,
                batch_size=self.batch_size.test,
                num_workers=self.num_workers.test,
                worker_init_fn=worker_init_fn,
            )
            for dataset in self.test_datasets
        ]

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}("
            f"self.dataset = {self.dataset}, "
            f"self.num_workers = {self.num_workers}, "
            f"self.batch_size = {self.batch_size})"
        )

    @staticmethod
    def precompute_data(dm : pl.LightningDataModule, new_root: str, create_parents= True, stage: Optional[str]= None):
        path = os.path.join(PROJECT_ROOT, new_root)
        if not os.path.exists(path):
            if not create_parents:
                raise ValueError(f"{path} does not exist")
            os.mkdir(path)
        dm.prepare_data()
        dm.setup(stage)
        dls = {}

        if stage is None or stage == "test":
            dls['test'] = dm.test_dataloader()
        if stage is None or stage == "fit":
            dls['val'] = dm.val_dataloader()
            dls['train'] = dm.train_dataloader()

        for split, dl in dls.items():
            hydra.utils.log.info(f"Saving split = {split}")
            compute_split(dl, path, split)
        hydra.utils.log.info(f"Precomputed all data!")

class ProgressParallel(Parallel):
    def __init__(self, use_tqdm=True, total=None, *args, **kwargs):
        self._use_tqdm = use_tqdm
        self._total = total
        super().__init__(*args, **kwargs)

    def __call__(self, *args, **kwargs):
        with tqdm(disable=not self._use_tqdm, total=self._total) as self._pbar:
            return Parallel.__call__(self, *args, **kwargs)

    def print_progress(self):
        if self._total is None:
            self._pbar.total = self.n_dispatched_tasks
        self._pbar.n = self.n_completed_tasks
        self._pbar.refresh()
           
def compute_split(dl: DataLoader, path,split:str):
    path_split = os.path.join(path, split)
    if not os.path.exists(path_split):
        os.mkdir(path_split)
    dl = dl[0] if isinstance(dl, list) else dl
    conv = dl.dataset.idx_to_class
    for _,l in conv.items():
        path_label = os.path.join(path_split, l)
        if not os.path.exists(path_label):
            os.mkdir(path_label)
    #batches = ProgressParallel(n_jobs=6, total=len(dl))(delayed(save_batch)(i,batch["video"], batch["label"], path_split, conv) for i,batch in enumerate(dl))
    with ProgressParallel(n_jobs=6, total=dl.batch_size) as accelerator:
        for i,batch in tqdm(enumerate(dl), desc=f"{split} dataloader", total=len(dl)):
            #save_batch(i,batch["video"], batch["label"], path_split, conv)
            videos, labels = batch["video"], batch["label"]
            accelerator(delayed(save_clip)(i,idx,video, conv[label.item()], path_split) for idx,video, label in zip(range(len(labels)),videos, labels))

def save_clip(batch: int, idx: int, video: torch.Tensor, label: str, path_split: str):
    path = os.path.join(path_split, label, f"clip_{batch + idx}.pt")
    if os.path.exists(path):
        return
    torch.save(video, path)


def save_batch(batch_idx: int,video: torch.Tensor, label: torch.Tensor, path_split: str, conv: Dict[int,str]):
    for i in range(len(label)):
        path_label = os.path.join(path_split, conv[label[i].item()])
        if os.path.exists(path_label):
            continue
        torch.save(video[i], os.path.join(path_label, f"clip_{batch_idx + i}.pt"))

@hydra.main(config_path=str(PROJECT_ROOT / "conf"), config_name="default")
def main(cfg: omegaconf.DictConfig):
    datamodule: pl.LightningDataModule = hydra.utils.instantiate(
        cfg.data.datamodule, _recursive_=False
    )
    #VideoDataModule.precompute_data(datamodule, f"data/k400p")

if __name__ == "__main__":
    main()
