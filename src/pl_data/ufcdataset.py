import pickle
from typing import Dict, Tuple, Optional, Callable, Any, List, cast

import os
import hydra
import torch
import omegaconf

from torch import Tensor
from torchvision.datasets import Kinetics

from src.common.utils import PROJECT_ROOT

from torchvision.datasets import VisionDataset
from torchvision.datasets.folder import find_classes, make_dataset
from torchvision.datasets.utils import verify_str_arg
from torchvision.datasets.video_utils import VideoClips

class UFCKinetics(VisionDataset):
    def __init__(
        self,
        root: str,
        frames_per_clip: int,
        split: str = "train",
        frame_rate: Optional[int] = None,
        step_between_clips: int = 1,
        transform: Optional[Callable] = None,
        extensions: Tuple[str, ...] = ("avi", "mp4"),
        num_workers: int = 1,
        _precomputed_metadata: Optional[Dict[str, Any]] = None,
        _video_width: int = 0,
        _video_height: int = 0,
        _video_min_dimension: int = 0,
        _audio_samples: int = 0,
        _audio_channels: int = 0,
        _force_metadata: bool = False,
    ) -> None:
        self.extensions = extensions

        self.root = root
    
        self.split_folder = os.path.join(root, split)
        self.split = verify_str_arg(split, arg="split", valid_values=["train", "val","test"])

        super().__init__(self.root)

        self.classes, class_to_idx = find_classes(self.split_folder)
        self.samples = make_dataset(self.split_folder, class_to_idx, extensions, is_valid_file=None)
        video_list = [x[0] for x in self.samples]
        if _precomputed_metadata is None and not _force_metadata:
            video_metadata = [
                os.path.join(self.split_folder, f"video_paths.pkl"),
                os.path.join(self.split_folder, f"video_pts.pt"),
                os.path.join(self.split_folder, f"video_fps.pkl"),
                ]
            if all(os.path.exists(x) for x in video_metadata):
                with open(video_metadata[0], "rb") as f:
                    paths = pickle.load(f)
                pts = torch.load(video_metadata[1])
                with open(video_metadata[2], "rb") as f:
                    fps = pickle.load(f)
                hydra.utils.log.info(f"Loading metadata from {video_metadata}")
                _precomputed_metadata = {
                    "video_paths": paths,
                    "video_pts": pts,
                    "video_fps": fps,
                }
        self.video_clips = VideoClips(
            video_list,
            frames_per_clip,
            step_between_clips,
            frame_rate,
            _precomputed_metadata,
            num_workers=num_workers,
            _video_width=_video_width,
            _video_height=_video_height,
            _video_min_dimension=_video_min_dimension,
            _audio_samples=_audio_samples,
            _audio_channels=_audio_channels,
        )
        if _precomputed_metadata is None:
            paths, pts, fps = self.video_clips.metadata["video_paths"], self.video_clips.metadata["video_pts"], self.video_clips.metadata["video_fps"]
            with open(os.path.join(self.split_folder, "video_paths.pkl"), "w+b") as f:
                pickle.dump(paths, f)
            torch.save(pts, os.path.join(self.split_folder, "video_pts.pt"))
            with open(os.path.join(self.split_folder, "video_fps.pkl"), "w+b") as f:
                pickle.dump(fps, f)
            hydra.utils.log.info(f"Saved metadata to {self.split_folder}")
        self.transform = transform

    @property
    def metadata(self) -> Dict[str, Any]:
        return self.video_clips.metadata

    def __len__(self) -> int:
        return self.video_clips.num_clips()

    def __getitem__(self, idx: int) -> Dict[Tensor, int]:
        video, _, _, video_idx = self.video_clips.get_clip(idx)
        # [T,H,W,C] --> [T,C,H,W]
        video = video.permute(0, 3, 1, 2)
        label = self.samples[video_idx][1]
        if self.transform is not None:
            video = self.transform(video)
        #video = (T, C, H, W)
        return {"video":video,"label": label}

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({self.root})\n{self.classes}"


@hydra.main(config_path=str(PROJECT_ROOT / "conf"), config_name="default")
def main(cfg: omegaconf.DictConfig):
    from torchvision import transforms
    transf = transforms.Compose([
            transforms.Resize((cfg.data.datamodule.img_size, cfg.data.datamodule.img_size)),
            transforms.ConvertImageDtype(torch.float),
        ])
    dataset: UFCKinetics = hydra.utils.instantiate(
        cfg.data.datamodule.dataset, split="train", transform= transf, _recursive_=False
    )


if __name__ == "__main__":
    main()
