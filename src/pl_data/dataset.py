from typing import Dict, Tuple, Optional, Callable, Any, List, cast

import os
import pickle

import hydra
import torch
import omegaconf
import dask.dataframe as dd

from torch import Tensor
from torchvision.datasets import VisionDataset
from torchvision.datasets.utils import verify_str_arg
from torchvision.datasets.folder import has_file_allowed_extension
from torchvision.datasets.video_utils import VideoClips

from src.common.utils import PROJECT_ROOT


class KineticsCSV(VisionDataset):
    """`Generic Kinetics <https://deepmind.com/research/open-source/open-source-datasets/kinetics/>`_
    dataset.

    Kinetics-400/600/700 are action recognition video datasets.
    This dataset consider every video as a collection of video clips of fixed size, specified
    by ``frames_per_clip``, where the step in frames between each clip is given by
    ``step_between_clips``.

    To give an example, for 2 videos with 10 and 15 frames respectively, if ``frames_per_clip=5``
    and ``step_between_clips=5``, the dataset size will be (2 + 3) = 5, where the first two
    elements will come from video 1, and the next three elements from video 2.
    Note that we drop clips which do not have exactly ``frames_per_clip`` elements, so not all
    frames in a video might be present.

    Args:
        root (string): Root directory of the Kinetics Dataset.
            Directory should be structured as follows:
                root/
                ├── dataset.csv (youtube_id, sec_start, sec_end, split, label)
                ├── video1.mp4
                ├── video2.mp4
                ├── video3.mp4

        frames_per_clip (int): number of frames in a clip
        split (str): split of the dataset to consider; ``"train"`` (default) ``"val"`` or ``"test"``
        frame_rate (float): If omitted, interpolate different frame rate for each clip.
        step_between_clips (int): number of frames between each clip
        transform (callable, optional): A function/transform that  takes in a TxHxWxC video
            and returns a transformed version.
        num_workers (int): Use multiple workers for VideoClips creation

    Returns:
        tuple: A dictionary with the following entries:

            - ``"video"`` (Tensor[T, C, H, W]): the `T` video frames in torch.uint8 tensor
            - ``"label"`` (int): class of the video clip
    """

    def __init__(
        self,
        root: str,
        frames_per_clip: int,
        split: str = "train",
        csv_file: Optional[str] = None,
        frame_rate: Optional[int] = None,
        step_between_clips: int = 1,
        transform: Optional[Callable] = None,
        channels_first: bool = True,
        extensions: List[str] = ["avi", "mp4"],
        num_workers: int = 1,
        _precomputed_metadata: Optional[Dict[str, str]] = None,
        _force_metadata: bool = False,
        _video_width: int = 0,
        _video_height: int = 0,
        _video_min_dimension: int = 0,
        _audio_samples: int = 0,
        _audio_channels: int = 0,
    ) -> None:

        self.extensions = extensions

        self.root = root
        self.channels_first = channels_first

        self.split = verify_str_arg(split, arg="split", valid_values=["train", "val", "test"])
        self.split_folder = os.path.join(PROJECT_ROOT, root, split)
        if csv_file is None:
            csv_file = f"{split}.csv"
        elif not csv_file.endswith(".csv"): 
            csv_file = f"{csv_file}.csv"
        self.csv_path = os.path.join(self.split_folder, csv_file)
        assert os.path.exists(self.csv_path), f"{self.csv_path} does not exist"
        
        super().__init__(self.root)
        data = dd.read_csv(self.csv_path, usecols=['label','youtube_id','time_start', 'time_end'])
        self.classes, class_to_idx = self.find_classes(data)
        self.idx_to_class = {v: k for k, v in class_to_idx.items()}
        self.samples = self.make_dataset(data, class_to_idx, extensions, is_valid_file=None)
        video_list = [x[0] for x in self.samples]
        if _precomputed_metadata is None and not _force_metadata:
            video_metadata = [
                os.path.join(self.split_folder, f"video_paths.pkl"),
                os.path.join(self.split_folder, f"video_pts.pt"),
                os.path.join(self.split_folder, f"video_fps.pkl"),
                ]
            if all(os.path.exists(x) for x in video_metadata):
                with open(video_metadata[0], "rb") as f:
                    paths = pickle.load(f)
                pts = torch.load(video_metadata[1])
                with open(video_metadata[2], "rb") as f:
                    fps = pickle.load(f)
                hydra.utils.log.info(f"Loading metadata from {video_metadata}")
                _precomputed_metadata = {
                    "video_paths": paths,
                    "video_pts": pts,
                    "video_fps": fps,
                }
        self.video_clips = VideoClips(
            video_list,
            frames_per_clip,
            step_between_clips,
            frame_rate,
            _precomputed_metadata,
            num_workers=num_workers,
            _video_width=_video_width,
            _video_height=_video_height,
            _video_min_dimension=_video_min_dimension,
            _audio_samples=_audio_samples,
            _audio_channels=_audio_channels,
        )
        if _precomputed_metadata is None:
            paths, pts, fps = self.video_clips.metadata["video_paths"], self.video_clips.metadata["video_pts"], self.video_clips.metadata["video_fps"]
            #print(f"type(paths) = {type(paths)}, type(pts) = {type(pts)}, type(fps) = {type(fps)}")
            #print(f"type(paths[0]) = {type(paths[0])}, type(pts[0]) = {type(pts[0])}, type(fps[0]) = {type(fps[0])}")
            with open(os.path.join(self.split_folder, "video_paths.pkl"), "w+b") as f:
                pickle.dump(paths, f)
            torch.save(pts, os.path.join(self.split_folder, "video_pts.pt"))
            with open(os.path.join(self.split_folder, "video_fps.pkl"), "w+b") as f:
                pickle.dump(fps, f)
            hydra.utils.log.info(f"Saved metadata to {self.split_folder}")
        self.transform = transform

    def find_classes(self, df : dd.DataFrame) -> Tuple[List[str], Dict[str, int]]:
        """Finds the class folders in a dataset.

        See :class:`DatasetFolder` for details.
        """
        classes = sorted(df.label.unique().compute())
        if not classes:
            raise FileNotFoundError(f"Couldn't find any class folder in {self.split_folder}.")

        class_to_idx = {cls_name: i for i, cls_name in enumerate(classes)}
        return classes, class_to_idx


    def make_dataset(
        self,
        df : dd.DataFrame,
        class_to_idx: Optional[Dict[str, int]] = None,
        extensions: Optional[List[str]] = None,
        is_valid_file: Optional[Callable[[str], bool]] = None,
        partial: bool = False,
    ) -> List[Tuple[str, int]]:
        """Generates a list of samples of a form (path_to_sample, class).

        See :class:`DatasetFolder` for details.

        Note: The class_to_idx parameter is here optional and will use the logic of the ``find_classes`` function
        by default.
        """

        if not class_to_idx:
            raise ValueError("'class_to_index' must have at least one entry to collect any samples.")

        both_none = extensions is None and is_valid_file is None
        both_something = extensions is not None and is_valid_file is not None
        if both_none or both_something:
            raise ValueError("Both extensions and is_valid_file cannot be None or not None at the same time")

        if extensions is not None:

            def is_valid_file(x: str) -> bool:
                return has_file_allowed_extension(x, cast(Tuple[str, ...], tuple(extensions)))

        is_valid_file = cast(Callable[[str], bool], is_valid_file)
        df['path'] = df.apply(lambda row: os.path.join(self.split_folder, f"{row.youtube_id}_{row.time_start:06d}_{row.time_end:06d}.mp4"),axis=1, meta=('path', 'str'))
        df = df[df["path"].apply(os.path.exists, meta=('e', 'bool'))]
        available_classes = df.label.unique().compute()
        empty_classes = set(class_to_idx.keys()) - set(available_classes)
        if empty_classes:
            msg = f"Found no valid file for the classes {', '.join(sorted(empty_classes))}. "
            if extensions is not None:
                msg += f"Supported extensions are: {', '.join(extensions)}"
            raise FileNotFoundError(msg)

        return [(row.path, class_to_idx[row.label]) for _, row in df.iterrows()]

    @property
    def metadata(self) -> Dict[str, Any]:
        return self.video_clips.metadata

    def __len__(self) -> int:
        return self.video_clips.num_clips()

    def __getitem__(self, idx: int) -> Dict[Tensor, int]:
        video, _, _, video_idx = self.video_clips.get_clip(idx)
        if self.channels_first:
            # [T,H,W,C] --> [T,C,H,W]
            video = video.permute(0, 3, 1, 2)
        label = self.samples[video_idx][1]
        if self.transform is not None:
            video = self.transform(video)
        #video = (T, C, H, W)
        return {"video":video,"label": label}
    
    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({self.root})\n{self.classes}"



@hydra.main(config_path=str(PROJECT_ROOT / "conf"), config_name="default")
def main(cfg: omegaconf.DictConfig):
    from torchvision import transforms
    transf = transforms.Compose([
            transforms.Resize((cfg.data.datamodule.img_size, cfg.data.datamodule.img_size)),
            transforms.Lambda(lambda x: x / 255.),
        ])
    dataset: KineticsCSV = hydra.utils.instantiate(
        cfg.data.datamodule.dataset, split="val", transform= transf, _recursive_=False
    )


if __name__ == "__main__":
    main()
