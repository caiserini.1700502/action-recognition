import torch
import hydra
import omegaconf
import pytorch_lightning as pl

from src.common.utils import PROJECT_ROOT
from pytorch_model_summary import summary

@hydra.main(config_path=str(PROJECT_ROOT / "conf"), config_name="default")
def main(cfg: omegaconf.DictConfig):
    model : pl.LightningModule= hydra.utils.instantiate(cfg.model, optim= cfg.optim, data=cfg.data,
        logging=cfg.logging,
        _recursive_=False,)
    x = torch.zeros(cfg.data.datamodule.batch_size.train,cfg.data.datamodule.dataset.frames_per_clip,3,cfg.data.datamodule.img_size,cfg.data.datamodule.img_size)
    summary(model,x, max_depth=4, show_parent_layers=False, print_summary=True)

if __name__ == "__main__":
    main()
