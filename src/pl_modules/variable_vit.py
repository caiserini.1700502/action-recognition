from typing import Any, Dict, List, Sequence, Tuple, Union, Optional

import hydra
import torch
import omegaconf
import pytorch_lightning as pl

from torch import Tensor, nn
from torch.optim import Optimizer
from torchmetrics import Accuracy, Precision, Recall, F1Score
from einops import rearrange, reduce
from torch.nn import functional as F


from src.common.utils import PROJECT_ROOT
from src.pl_modules.vit_layers import PatchEmbed, Block, AttentionType


class VariableVit(pl.LightningModule):
    """Simplified implementation of the Vision transformer.
    Parameters
    ----------
    img_size : int
        Both height and the width of the image (it is a square).
    patch_size : int
        Both height and the width of the patch (it is a square).
    in_chans : int
        Number of input channels.
    n_classes : int
        Number of classes.
    embed_dim : int
        Dimensionality of the token/patch embeddings.
    depth : int
        Number of blocks.
    n_heads : int
        Number of attention heads.
    mlp_ratio : float
        Determines the hidden dimension of the `MLP` module.
    qkv_bias : bool
        If True then we include bias to the query, key and value projections.
    p, attn_p : float
        Dropout probability.
    Attributes
    ----------
    patch_embed : PatchEmbed
        Instance of `PatchEmbed` layer.
    cls_token : nn.Parameter
        Learnable parameter that will represent the first token in the sequence.
        It has `embed_dim` elements.
    pos_emb : nn.Parameter
        Positional embedding of the cls token + all the patches.
        It has `(n_patches + 1) * embed_dim` elements.
    pos_drop : nn.Dropout
        Dropout layer.
    blocks : nn.ModuleList
        List of `Block` modules.
    norm : nn.LayerNorm
        Layer normalization.
    """

    CLASSES = ["ApplyEyeMakeup","CleanAndJerk","HorseRace","PlayingDhol","Skiing","YoYo","ApplyLipstick","CliffDiving","HorseRiding","PlayingFlute","Skijet","Archery","CricketBowling","HulaHoop","PlayingGuitar","SkyDiving","BabyCrawling","CricketShot","IceDancing","PlayingPiano","SoccerJuggling","BalanceBeam","CuttingInKitchen","JavelinThrow","PlayingSitar","SoccerPenalty","BandMarching","Diving","JugglingBalls","PlayingTabla","StillRings","BaseballPitch","Drumming","JumpingJack","PlayingViolin","SumoWrestling","Basketball","Fencing","JumpRope","PoleVault","Surfing","BasketballDunk","FieldHockeyPenalty","Kayaking","PommelHorse","Swing","BenchPress","FloorGymnastics","Knitting","PullUps","TableTennisShot","Biking","FrisbeeCatch","LongJump","Punch","TaiChi","Billiards","FrontCrawl","Lunges","PushUps","TennisSwing","BlowDryHair","GolfSwing","MilitaryParade","Rafting","ThrowDiscus","BlowingCandles","Haircut","Mixing","RockClimbingIndoor","TrampolineJumping","BodyWeightSquats","Hammering","MoppingFloor","RopeClimbing","Typing","Bowling","HammerThrow","Nunchucks","Rowing","UnevenBars","BoxingPunchingBag","HandstandPushups","ParallelBars","SalsaSpin","VolleyballSpiking","BoxingSpeedBag","HandstandWalking","PizzaTossing","ShavingBeard","WalkingWithDog","BreastStroke","HeadMassage","PlayingCello","Shotput","WallPushups","BrushingTeeth","HighJump","PlayingDaf","SkateBoarding","WritingOnBoard"]

    def __init__(
        self,
        patch_size: Union[int, Tuple[int, int]] = 16,
        in_chans=3,
        num_classes=400,
        embed_dim=384,
        depth=2,
        n_heads: Union[int, List[int]] = 12,
        s_heads: Union[int, List[int]] = 2,
        mlp_ratio=4.0,
        qkv_bias=False,
        qk_scale: Optional[float] = None,
        lin_qkv=False,
        drop_rate=0.0,
        attn_drop_rate=0.0,
        drop_path_rate=0.1,
        norm_layer: nn.Module = nn.LayerNorm,
        num_frames=10,
        attention_type: AttentionType = AttentionType.DIVIDED,
        dropout=0.0,
        *args,
        **kwargs,
    ) -> None:

        super().__init__()
        self.save_hyperparameters()  # populate self.hparams with args and kwargs automagically!

        img_size = self.hparams.data.datamodule.img_size
        num_frames = self.hparams.data.datamodule.dataset.frames_per_clip

        self.attention_type = attention_type
        self.dropout = nn.Dropout(dropout)
        self.embed_dim = embed_dim  # num_features for consistency with other models
        img_size = to_tuple(img_size)
        patch_size = to_tuple(patch_size)
        assert (
            img_size[0] % patch_size[0] == 0 and img_size[0] % patch_size[0] == 0
        ), f"img_size = {img_size} must be divisible by patch_size = {patch_size}"

        self.patch_embed = PatchEmbed(
            patch_size=patch_size,
            in_chans=in_chans,
            embed_dim=embed_dim,
            frames=num_frames,
        )
        # calcola patches qui
        n_patches = (img_size[0] // patch_size[0]) * (img_size[1] // patch_size[1])

        ## Positional Embeddings
        self.cls_token = nn.Parameter(torch.zeros(1, embed_dim))
        self.pos_embed = nn.Parameter(torch.zeros(n_patches + 1, self.embed_dim))
        self.pos_drop = nn.Dropout(p=drop_rate)

        if self.attention_type != AttentionType.SPATIAL:
            self.time_embed = nn.Parameter(torch.zeros(num_frames, embed_dim))
            self.time_drop = nn.Dropout(p=drop_rate)

        ## Attention Blocks
        dpr = torch.linspace(0, drop_path_rate, depth)  # stochastic depth decay rule
        n_heads = check_heads(depth, n_heads)
        s_heads = check_heads(depth, s_heads)

        assert all(
            self.embed_dim % n == 0 for n in n_heads
        ), f"embed_dim {self.embed_dim} must be divisible by all heads {n_heads}"
        assert all(
            num_frames % s == 0 for s in s_heads
        ), f"num_frames {num_frames} must be divisible by all heads {s_heads}"

        self.blocks = nn.ModuleList(
            [
                Block(
                    dim=embed_dim,
                    n_heads=h,
                    s_heads=s,
                    mlp_ratio=mlp_ratio,
                    qkv_bias=qkv_bias,
                    qk_scale=qk_scale,
                    lin_qkv=lin_qkv,
                    proj_drop=drop_rate,
                    attn_drop=attn_drop_rate,
                    drop_path=d.item(),
                    norm_layer=norm_layer,
                    attention_type=self.attention_type,
                )
                for d, h, s in zip(dpr, n_heads, s_heads)
            ]
        )
        self.norm = norm_layer(embed_dim)

        # Classifier head
        self.head = (
            nn.Linear(embed_dim, num_classes) if num_classes > 0 else nn.Identity()
        )

        self.acc = Accuracy(num_classes=num_classes)
        self.prec = Precision(num_classes=num_classes, average="macro")
        self.recall = Recall(num_classes=num_classes, average="macro")
        self.F1Score = F1Score(num_classes=num_classes, average="macro") 

        self.val_acc = Accuracy(num_classes=num_classes)
        self.val_prec = Precision(num_classes=num_classes, average="macro")
        self.val_recall = Recall(num_classes=num_classes, average="macro")
        self.val_F1Score = F1Score(num_classes=num_classes, average="macro")


    def forward_features(self, x: Tensor) -> Tensor:
        x = self.patch_embed(x)
        mask_tokens = x.shape[:-2]
        cls_tokens = self.cls_token.expand(*mask_tokens, -1, -1)
        x = torch.cat((cls_tokens, x), dim=-2)
        x = x + self.pos_embed
        x = self.pos_drop(x)

        ## Time Embeddings
        if self.attention_type != AttentionType.SPATIAL:
            xt = x[..., 1:, :]
            xt = rearrange(xt, "... t n m -> ... n t m")
            xt = xt + self.time_embed
            xt = self.time_drop(xt)
            xt = rearrange(xt, "... n t m -> ... t n m")
            x = torch.cat([x[..., 0, :].unsqueeze(-2), xt], dim=-2)

        ## Attention blocks
        for blk in self.blocks:
            x = blk(x)

        ### Predictions for space-only baseline
        # if self.attention_type == AttentionType.SPATIAL:
        x = reduce(x, "... t n m -> ... n m", "mean")

        x = self.norm(x)
        return x[..., 0, :]

    def forward(self, x: Tensor) -> Dict[str, Tensor]:
        x = self.forward_features(x)
        x = self.head(x)
        return x

    def training_step(self, batch: Any, batch_idx: int) -> torch.Tensor:
        x, truth = batch["video"], batch["label"]
        x = self(x)

        loss = F.cross_entropy(x, truth)
        self.acc(x, truth)
        self.prec(x, truth)
        self.recall(x, truth)
        self.F1Score(x, truth)
        self.log_dict(
            {
                "train_acc": self.acc,
                "train_prec": self.prec,
                "train_recall": self.recall,
                "train_F1Score": self.F1Score,
            },
            on_step=False,
            on_epoch=True,
            prog_bar=True,
        )
        return loss


    def validation_step(self, batch: Any, batch_idx: int) -> torch.Tensor:
        x, truth = batch["video"], batch["label"]
        x = self(x)

        loss = F.cross_entropy(x, truth)
        self.val_acc(x, truth)
        self.val_prec(x, truth)
        self.val_recall(x, truth)
        self.val_F1Score(x, truth)
        self.log_dict(
            {
                "val_loss": loss,
                "val_acc": self.val_acc,
                "val_prec": self.val_prec,
                "val_recall": self.val_recall,
                "val_F1Score": self.val_F1Score,
            },
            on_step=False,
            on_epoch=True,
            prog_bar=True,
        )
        return loss

    def test_step(self, batch: Any, batch_idx: int) -> torch.Tensor:
        x, truth = batch["video"], batch["label"]
        x = self(x)

        loss = F.cross_entropy(x, truth)

        self.acc(x, truth)
        self.prec(x, truth)
        self.recall(x, truth)
        self.F1Score(x, truth)
        self.log_dict(
            {
                "train_acc": self.acc,
                "train_prec": self.prec,
                "train_recall": self.recall,
                "train_F1Score": self.F1Score,
            },
            on_step=False,
            on_epoch=True,
            prog_bar=False,
        )
        return loss


    def configure_optimizers(
        self,
    ) -> Union[Optimizer, Tuple[Sequence[Optimizer], Sequence[Any]]]:
        """
        Choose what optimizers and learning-rate schedulers to use in your optimization.
        Normally you'd need one. But in the case of GANs or similar you might have multiple.
        Return:
            Any of these 6 options.
            - Single optimizer.
            - List or Tuple - List of optimizers.
            - Two lists - The first list has multiple optimizers, the second a list of LR schedulers (or lr_dict).
            - Dictionary, with an 'optimizer' key, and (optionally) a 'lr_scheduler'
              key whose value is a single LR scheduler or lr_dict.
            - Tuple of dictionaries as described, with an optional 'frequency' key.
            - None - Fit will run without any optimizer.
        """

        opt = hydra.utils.instantiate(
            self.hparams.optim.optimizer, params=self.parameters(), _convert_="partial"
        )
        if not self.hparams.optim.use_lr_scheduler:
            return [opt]
        scheduler = hydra.utils.instantiate(
            self.hparams.optim.lr_scheduler, optimizer=opt
        )
        return [opt], [scheduler]


def check_heads(depth, heads):
    if isinstance(heads, int):
        heads = [heads] * depth
    elif len(heads) > depth:
        raise ValueError(f"Too many heads provided! Heads {len(heads)} > Depth {depth}")
    elif len(heads) < depth:
        heads = heads[:-1] + heads[-1] * (len(heads) - depth)
    return heads


def to_tuple(img_size):
    if isinstance(img_size, int):
        img_size = (img_size, img_size)
    return img_size


@hydra.main(config_path=str(PROJECT_ROOT / "conf"), config_name="default")
def main(cfg: omegaconf.DictConfig):
    model: pl.LightningModule = hydra.utils.instantiate(
        cfg.model,
        optim=cfg.optim,
        data=cfg.data,
        logging=cfg.logging,
        _recursive_=False,
    )


if __name__ == "__main__":
    main()
