from typing import Any, Dict, List, Sequence, Tuple, Union

import hydra
import torch
import omegaconf
import pytorch_lightning as pl

from torch import Tensor, nn
from torch.optim import Optimizer
from torchmetrics import Accuracy, Precision, Recall, F1Score
from torch.nn import functional as F


from src.common.utils import PROJECT_ROOT
from src.pl_modules.vidtr_layers import TransformerEncoderLayer, TransformerEncoder


class VidTr(pl.LightningModule):
    """Simplified implementation of the Vision transformer.
    Parameters
    ----------
    img_size : int
        Both height and the width of the image (it is a square).
    patch_size : int
        Both height and the width of the patch (it is a square).
    in_chans : int
        Number of input channels.
    n_classes : int
        Number of classes.
    embed_dim : int
        Dimensionality of the token/patch embeddings.
    depth : int
        Number of blocks.
    n_heads : int
        Number of attention heads.
    mlp_ratio : float
        Determines the hidden dimension of the `MLP` module.
    qkv_bias : bool
        If True then we include bias to the query, key and value projections.
    p, attn_p : float
        Dropout probability.
    Attributes
    ----------
    patch_embed : PatchEmbed
        Instance of `PatchEmbed` layer.
    cls_token : nn.Parameter
        Learnable parameter that will represent the first token in the sequence.
        It has `embed_dim` elements.
    pos_emb : nn.Parameter
        Positional embedding of the cls token + all the patches.
        It has `(n_patches + 1) * embed_dim` elements.
    pos_drop : nn.Dropout
        Dropout layer.
    blocks : nn.ModuleList
        List of `Block` modules.
    norm : nn.LayerNorm
        Layer normalization.
    """

    CLASSES = [
        "ApplyEyeMakeup",
        "CleanAndJerk",
        "HorseRace",
        "PlayingDhol",
        "Skiing",
        "YoYo",
        "ApplyLipstick",
        "CliffDiving",
        "HorseRiding",
        "PlayingFlute",
        "Skijet",
        "Archery",
        "CricketBowling",
        "HulaHoop",
        "PlayingGuitar",
        "SkyDiving",
        "BabyCrawling",
        "CricketShot",
        "IceDancing",
        "PlayingPiano",
        "SoccerJuggling",
        "BalanceBeam",
        "CuttingInKitchen",
        "JavelinThrow",
        "PlayingSitar",
        "SoccerPenalty",
        "BandMarching",
        "Diving",
        "JugglingBalls",
        "PlayingTabla",
        "StillRings",
        "BaseballPitch",
        "Drumming",
        "JumpingJack",
        "PlayingViolin",
        "SumoWrestling",
        "Basketball",
        "Fencing",
        "JumpRope",
        "PoleVault",
        "Surfing",
        "BasketballDunk",
        "FieldHockeyPenalty",
        "Kayaking",
        "PommelHorse",
        "Swing",
        "BenchPress",
        "FloorGymnastics",
        "Knitting",
        "PullUps",
        "TableTennisShot",
        "Biking",
        "FrisbeeCatch",
        "LongJump",
        "Punch",
        "TaiChi",
        "Billiards",
        "FrontCrawl",
        "Lunges",
        "PushUps",
        "TennisSwing",
        "BlowDryHair",
        "GolfSwing",
        "MilitaryParade",
        "Rafting",
        "ThrowDiscus",
        "BlowingCandles",
        "Haircut",
        "Mixing",
        "RockClimbingIndoor",
        "TrampolineJumping",
        "BodyWeightSquats",
        "Hammering",
        "MoppingFloor",
        "RopeClimbing",
        "Typing",
        "Bowling",
        "HammerThrow",
        "Nunchucks",
        "Rowing",
        "UnevenBars",
        "BoxingPunchingBag",
        "HandstandPushups",
        "ParallelBars",
        "SalsaSpin",
        "VolleyballSpiking",
        "BoxingSpeedBag",
        "HandstandWalking",
        "PizzaTossing",
        "ShavingBeard",
        "WalkingWithDog",
        "BreastStroke",
        "HeadMassage",
        "PlayingCello",
        "Shotput",
        "WallPushups",
        "BrushingTeeth",
        "HighJump",
        "PlayingDaf",
        "SkateBoarding",
        "WritingOnBoard",
    ]

    def __init__(
        self,
        d_model=512,
        nhead=8,
        num_encoder_layers=6,
        dim_feedforward=2048,
        dropout=0.1,
        normalize_before=False,
        patch_size=(1, 16, 16),
        in_channel=3,
        classes=101,
        merge_index=6,
        merge_later=False,
        *args,
        **kwargs,
    ):
        super().__init__()
        self.save_hyperparameters()  # populate self.hparams with args and kwargs automagically!

        img_size = self.hparams.data.datamodule.img_size
        self.temporal_size = self.hparams.data.datamodule.dataset.frames_per_clip
        self.merge_later = merge_later

        self.conv_stem = nn.Conv3d(
            in_channels=in_channel,
            out_channels=d_model,
            kernel_size=patch_size,
            stride=patch_size,
            bias=True,
        )
        pos_embedding_layer_wise = True if self.temporal_size == 16 else False
        layer_list = []
        for i in range(num_encoder_layers):
            module_temp = TransformerEncoderLayer(
                d_model,
                nhead,
                dim_feedforward,
                dropout,
                normalize_before,
                merge_index=merge_index,
                merge_later=merge_later,
                layer_index=i,
                pos_embedding_layer_wise=pos_embedding_layer_wise,
            )
            layer_list.append(module_temp)
        encoder_layers = nn.ModuleList(layer_list)

        encoder_norm = nn.LayerNorm(d_model) if normalize_before else None
        self.encoder = TransformerEncoder(
            encoder_layers, num_encoder_layers, encoder_norm
        )

        self._reset_parameters()

        self.d_model = d_model
        self.nhead = nhead
        self.avg_pool = nn.AdaptiveAvgPool3d((1, 1, 1))
        self.fc = nn.Linear(in_features=d_model, out_features=classes, bias=True)

        img_size = to_tuple(img_size)
        n_patches = (img_size[0] // patch_size[1]) * (img_size[1] // patch_size[2])
        if merge_later:
            self.cls = nn.Parameter(torch.Tensor(1, 1, d_model))
            self.pos_embedding = nn.Parameter(
                torch.Tensor(1, self.temporal_size * n_patches + 1, self.d_model)
            )
        else:
            self.pos_embedding = nn.Parameter(
                torch.Tensor(
                    1, (self.temporal_size + 1) * (n_patches + 1), self.d_model
                )
            )
            self.cls_s = nn.Parameter(torch.Tensor(self.temporal_size, 1, d_model))
            self.cls_t = nn.Parameter(torch.Tensor(1, n_patches + 1, d_model))
        self.dropout = nn.Dropout(0.5)
        self.dp_pos = nn.Dropout(0.1)

        self.acc = Accuracy(num_classes=classes)
        self.prec = Precision(num_classes=classes, average="macro")
        self.recall = Recall(num_classes=classes, average="macro")
        self.f1 = F1Score(num_classes=classes, average="macro")

        self.val_acc = Accuracy(num_classes=classes)
        self.val_prec = Precision(num_classes=classes, average="macro")
        self.val_recall = Recall(num_classes=classes, average="macro")
        self.val_f1 = F1Score(num_classes=classes, average="macro")

    def _reset_parameters(self):
        for p in self.parameters():
            if p.dim() > 1:
                nn.init.xavier_uniform_(p)

    def forward(self, src: Tensor) -> Tensor:
        # (B, T, C, H, W)
        src = src.permute(0, 2, 1, 3, 4)
        # (B, C, T, H, W)
        src = self.conv_stem(src)
        # (B, F, T, H, W)
        bs, c, t, h, w = src.shape

        # (B, F, T, H, W)
        pos_embed = self.pos_embedding.permute((1, 0, 2)).repeat(1, bs, 1)

        if self.merge_later:
            src = src.flatten(2).permute(2, 0, 1)
            cls = self.cls.repeat(1, bs, 1)
            src = torch.cat((cls, src), dim=0)
        else:
            src = src.flatten(3).permute(2, 3, 0, 1)
            cls_t = self.cls_t.view(1, w * h + 1, 1, self.d_model).repeat(1, 1, bs, 1)
            cls_s = self.cls_s.view(t, 1, 1, self.d_model).repeat(1, 1, bs, 1)

            src = torch.cat((cls_s, src), dim=1)
            src = torch.cat((cls_t, src), dim=0)
        src = src.view(-1, bs, self.d_model)

        memory = self.encoder(
            src, (bs, c, t, h, w), src_key_padding_mask=None, pos=pos_embed
        )

        out = memory.view(t + 1, w * h + 1, bs, c)[0, 0, :, :] if not self.merge_later else memory[0, :, :]
        
        out = self.dropout(out.view(bs, -1))
        out = self.fc(out)

        return out

    def training_step(self, batch: Any, batch_idx: int) -> Tensor:
        x, truth = batch["video"], batch["label"]
        x = self(x)

        loss = F.cross_entropy(x, truth)
        self.acc(x, truth)
        self.prec(x, truth)
        self.recall(x, truth)
        self.f1(x, truth)
        self.log_dict(
            {
                "train_acc" : self.acc,
                "train_prec": self.prec,
                "train_recall": self.recall,
                "train_f1": self.f1,
            },
            on_step=False,
            on_epoch=True,
            prog_bar=False,
        )
        return loss

    def validation_step(self, batch: Any, batch_idx: int) -> Tensor:
        x, truth = batch["video"], batch["label"]
        x = self(x)

        loss = F.cross_entropy(x, truth)
        self.val_acc(x, truth)
        self.val_prec(x, truth)
        self.val_recall(x, truth)
        self.val_f1(x, truth)
        self.log_dict(
            {   
                "val_loss": loss,
                "val_acc": self.val_acc,
                "val_prec": self.val_prec,
                "val_recall": self.val_recall,
                "val_f1": self.val_f1,
            },
            on_step=False,
            on_epoch=True,
            prog_bar=False,
        )
        return loss

    def test_step(self, batch: Any, batch_idx: int) -> Tensor:
        x, truth = batch["video"], batch["label"]
        x = self(x)

        loss = F.cross_entropy(x, truth)

        self.acc(x, truth)
        self.acc_classes(x, truth)
        self.top_acc(x, truth)
        self.log_dict(
            {
                "Test acc": self.acc,
                "Test acc top 5": self.top_acc,
                "Test acc class": self.acc_classes,
            },
            on_step=False,
            on_epoch=True,
        )
        return loss

    def configure_optimizers(
        self,
    ) -> Union[Optimizer, Tuple[Sequence[Optimizer], Sequence[Any]]]:
        """
        Choose what optimizers and learning-rate schedulers to use in your optimization.
        Normally you'd need one. But in the case of GANs or similar you might have multiple.
        Return:
            Any of these 6 options.
            - Single optimizer.
            - List or Tuple - List of optimizers.
            - Two lists - The first list has multiple optimizers, the second a list of LR schedulers (or lr_dict).
            - Dictionary, with an 'optimizer' key, and (optionally) a 'lr_scheduler'
              key whose value is a single LR scheduler or lr_dict.
            - Tuple of dictionaries as described, with an optional 'frequency' key.
            - None - Fit will run without any optimizer.
        """

        opt = hydra.utils.instantiate(
            self.hparams.optim.optimizer, params=self.parameters(), _convert_="partial"
        )
        if not self.hparams.optim.use_lr_scheduler:
            return [opt]
        scheduler = hydra.utils.instantiate(
            self.hparams.optim.lr_scheduler, optimizer=opt
        )
        return [opt], [scheduler]


def to_tuple(
    img_size,
) -> Tuple[int,]:
    if isinstance(img_size, int):
        img_size = (img_size, img_size)
    elif isinstance(img_size, list):
        img_size = tuple(img_size)
    return img_size


@hydra.main(config_path=str(PROJECT_ROOT / "conf"), config_name="default")
def main(cfg: omegaconf.DictConfig):
    model: pl.LightningModule = hydra.utils.instantiate(
        cfg.model,
        optim=cfg.optim,
        data=cfg.data,
        logging=cfg.logging,
        _recursive_=False,
    )


if __name__ == "__main__":
    main()
