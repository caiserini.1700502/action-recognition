from torch import nn
from enum import Enum
from torch import Tensor
from einops import rearrange
from typing import  Optional, Tuple

import torch

class AttentionType(Enum):
    """Types of attention.
    """

    SPATIAL = "space_only"
    DIVIDED = "divided_space_time"

class Mlp(nn.Module):
    """Multilayer perceptron.
    Parameters
    ----------
    in_features : int
        Number of input features.
    hidden_features : Optional[int]
        Number of nodes in the hidden layer.
    out_features : Optional[int]
        Number of output features.
    act_layer : nn.Module = nn.GELU
        Activation function after first Linear Layer.
    p : float = 0.2
        Dropout probability.
    Attributes
    ----------
    fc1 : nn.Linear
        The First linear layer.
    act : nn.GELU
        GELU activation function.
    fc2 : nn.Linear
        The second linear layer.
    drop : nn.Dropout
        Dropout layer.
    """
    def __init__(
        self,
        in_features: int,
        hidden_features: Optional[int] = None,
        out_features: Optional[int] = None,
        act_layer: nn.Module = nn.GELU,
        drop: float = 0.2,
    ):
        super().__init__()
        out_features = out_features or in_features
        hidden_features = hidden_features or in_features
        self.fc1 = nn.Linear(in_features, hidden_features)
        self.act = act_layer()
        self.fc2 = nn.Linear(hidden_features, out_features)
        self.drop = nn.Dropout(drop)

    def forward(self, x: Tensor) -> Tensor:
        x = self.fc1(x)
        x = self.act(x)
        x = self.drop(x)
        x = self.fc2(x)
        x = self.drop(x)
        return x

class LinearImage(nn.Module):
    __constants__ = ['in_features', 'out_features']
    in_features: int
    out_features: int
    weight: Tensor

    def __init__(self, image: Tuple[int,int], out_features: int, bias: bool = True,
                 channel_last=False) -> None:
        super(LinearImage, self).__init__()
        #self.in_features = in_features
        self.out_features = out_features
        self.weight = nn.Parameter(torch.empty((out_features, *image)))
        if bias:
            self.bias = nn.Parameter(torch.empty(out_features,1,1))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()
    
    def reset_parameters(self) -> None:
        # Setting a=sqrt(5) in kaiming_uniform is the same as initializing with
        # uniform(-1/sqrt(in_features), 1/sqrt(in_features)). For details, see
        # https://github.com/pytorch/pytorch/issues/57109
        nn.init.xavier_uniform_(self.weight, gain=nn.init.calculate_gain('conv3d'))
        if self.bias is not None:
            fan_in, _ = nn.init._calculate_fan_in_and_fan_out(self.weight)
            bound = 1 / fan_in**0.5 if fan_in > 0 else 0
            nn.init.uniform_(self.bias, -bound, bound)

    def forward(self, x: Tensor) -> Tensor:
        x = torch.einsum('... i h w, o h w -> o h w ', x, self.weight)
        if self.bias is not None:
            x = x + self.bias
        return x

class AttentionImage(nn.Module):
    def __init__(
        self,
        dim: int,
        n_heads=8,
        s_heads=1,
        qkv_bias=False,
        qk_scale: Optional[float] = None,
        attn_drop=0.,
        proj_drop=0.,
    ):
        super().__init__()
        self.n_heads = n_heads
        self.s_heads = s_heads
        self.head_dim = dim // n_heads
        self.scale = qk_scale or self.head_dim ** -0.5
        self.qkv = nn.Linear(dim, dim * 3, bias=qkv_bias)
        self.proj = nn.Linear(dim, dim)
        self.proj_drop = nn.Dropout(proj_drop)
        self.attn_drop = nn.Dropout(attn_drop)

    def forward(self, x: Tensor) -> Tensor:
        #(B, T, P, D)
        # remember P is patches + 1
        #B, P, F = x.shape
        # (T, D) -> (HT, T//HT, D)
        x = rearrange(x, '... (s p) f -> ... s p f', s=self.s_heads)
        # (b,t, n, 3 * f)
        qkv = self.qkv(x)

        # (batch,t, 3, n_heads, patches, head_dim)
        qkv = rearrange(
            qkv, "... p (q n h) -> ... q n p h",q=3,n=self.n_heads, h=self.head_dim
        )

        # (batch, n_heads, patches, head_dim)
        q, k, v = qkv[...,0,:,:,:], qkv[...,1,:,:,:], qkv[...,2,:,:,:]
        # (batch, time, n_heads, head_dim, patches)
        k = rearrange(k, "... p h -> ... h p")
        # (batch, time, n_heads, patches, patches)
        attn = (q @ k) * self.scale
        # (batch, time, n_heads, patches, patches)
        attn = attn.softmax(dim=-1)
        # (batch, time, n_heads, patches, patches)
        attn = self.attn_drop(attn) 

        # (batch, time, n_heads, patches, head_dim)
        x = attn @ v
        # (batch, time, patches, dim)
        x = rearrange(x, "... s n p h -> ... (s p) (n h)")

        if self.lin_qkv:
            # (batch, time, patches, dim)
            x = self.proj(x)
            # (batch, time, patches, dim)
            x = self.proj_drop(x)

        return x
        

class Attention(nn.Module):
    """Attention mechanism.
    Parameters
    ----------
    dim : int
        The input and out dimension of per token features.
    n_heads : int = 8
        Number of attention heads.
    qkv_bias : bool = False
        If True then we include bias to the query, key and value projections.
    attn_p : Optional[float]
        Dropout probability applied to the query, key and value tensors.
    proj_p : float = 0.
        Dropout probability applied to the output tensor.
    Attributes
    ----------
    scale : float
        Normalizing consant for the dot product.
    qkv : nn.Linear
        Linear projection for the query, key and value.
    proj : nn.Linear
        Linear mapping that takes in the concatenated output of all attention
        heads and maps it into a new space.
    attn_drop, proj_drop : nn.Dropout
        Dropout layers.
    """
    def __init__(
        self,
        dim: int,
        n_heads=8,
        s_heads=1,
        qkv_bias=False,
        qk_scale: Optional[float] = None,
        lin_qkv = False,
        attn_drop=0.,
        proj_drop=0.,
    ):
        super().__init__()
        self.n_heads = n_heads
        self.s_heads = s_heads
        self.head_dim = dim // n_heads
        self.scale = qk_scale or self.head_dim ** -0.5
        self.lin_qkv = lin_qkv
        if lin_qkv:
            self.qkv = nn.Linear(dim, dim * 3, bias=qkv_bias)
            self.proj = nn.Linear(dim, dim)
            self.proj_drop = nn.Dropout(proj_drop)
        self.attn_drop = nn.Dropout(attn_drop)

    def forward(self, x: Tensor) -> Tensor:
        #(B, T, P, D)
        # remember P is patches + 1
        #B, P, F = x.shape
        # (T, D) -> (HT, T//HT, D)
        x = rearrange(x, '... (s p) f -> ... s p f', s=self.s_heads)
        if self.lin_qkv:
            # (b,t, n, 3 * f)
            qkv = self.qkv(x)

            # (batch,t, 3, n_heads, patches, head_dim)
            qkv = rearrange(
                qkv, "... p (q n h) -> ... q n p h",q=3,n=self.n_heads, h=self.head_dim
            )

            # (batch, n_heads, patches, head_dim)
            q, k, v = qkv[...,0,:,:,:], qkv[...,1,:,:,:], qkv[...,2,:,:,:]
        else:
            # (batch,t, n_heads, patches, head_dim)
            qkv = rearrange(
                x, "... p (n h) -> ... n p h", n=self.n_heads, h=self.head_dim
            )
            q, k, v = qkv,qkv,qkv
        # (batch, time, n_heads, head_dim, patches)
        k = rearrange(k, "... p h -> ... h p")
        # (batch, time, n_heads, patches, patches)
        attn = (q @ k) * self.scale
        # (batch, time, n_heads, patches, patches)
        attn = attn.softmax(dim=-1)
        # (batch, time, n_heads, patches, patches)
        attn = self.attn_drop(attn) 

        # (batch, time, n_heads, patches, head_dim)
        x = attn @ v
        # (batch, time, patches, dim)
        x = rearrange(x, "... s n p h -> ... (s p) (n h)")

        if self.lin_qkv:
            # (batch, time, patches, dim)
            x = self.proj(x)
            # (batch, time, patches, dim)
            x = self.proj_drop(x)

        return x

class Block(nn.Module):
    """Transformer block.
    Parameters
    ----------
    dim : int
        Embeddinig dimension.
    n_heads : int
        Number of attention heads.
    mlp_ratio : float
        Determines the hidden dimension size of the `MLP` module with respect
        to `dim`.
    qkv_bias : bool
        If True then we include bias to the query, key and value projections.
    p, attn_p : float
        Dropout probability.
    Attributes
    ----------
    norm1, norm2 : LayerNorm
        Layer normalization.
    attn : Attention
        Attention module.
    mlp : MLP
        MLP module.
    """
    def __init__(
        self,
        dim: int,
        n_heads: int,
        s_heads: int,
        mlp_ratio: Optional[float] = 4.0,
        qkv_bias=False,
        qk_scale: Optional[float]=None,
        lin_qkv=False,
        proj_drop=0.0,
        attn_drop=0.2,
        drop_path: Optional[float]=0.1,
        act_layer: nn.Module = nn.GELU,
        norm_layer: nn.Module = nn.LayerNorm,
        attention_type: AttentionType=AttentionType.DIVIDED,
    ):
        super().__init__()
        self.attention_type = attention_type

        self.space_norm = norm_layer(dim)# nn.LayerNorm
        self.attn = Attention(
            dim,
            n_heads=n_heads,
            #s_heads=s_heads,
            qkv_bias=qkv_bias,
            qk_scale=qk_scale,
            lin_qkv=lin_qkv,
            attn_drop=attn_drop,
            proj_drop=proj_drop,
        )

        self.temporal_norm = norm_layer(dim)
        self.temporal_attn = Attention(
            dim,
            n_heads=n_heads,
            s_heads=s_heads,
            qkv_bias=qkv_bias,
            qk_scale=qk_scale,
            lin_qkv=lin_qkv,
            attn_drop=attn_drop,
            proj_drop=proj_drop,
        )
        self.temporal_fc = nn.Linear(dim, dim)

        ## drop path
        self.drop_path = nn.Dropout2d(drop_path)
        self.norm = norm_layer(dim)
        mlp_hidden_dim = int(dim * mlp_ratio) if mlp_ratio is not None else mlp_ratio
        self.mlp = Mlp(
            in_features=dim,
            hidden_features=mlp_hidden_dim,
            act_layer=act_layer,
            drop=proj_drop,
        )

    def forward(self, x: Tensor) -> Tensor:
        if self.attention_type == AttentionType.DIVIDED:
            ## Temporal
            xt = x[..., 1:, :]
            xt = rearrange(xt, "... t n m -> ... n t m")
            res_temp = self.temporal_norm(xt)
            res_temp = self.temporal_attn(res_temp)
            res_temp = self.drop_path(res_temp)
            #res_temp = self.temporal_fc(res_temp)
            xt = xt + res_temp
            xt = rearrange(xt, "... n t m -> ... t n m")
            x = torch.cat([x[..., 0, :].unsqueeze(-2), xt], dim=-2)


        ## Spatial
        res_space = self.space_norm(x)
        res_space = self.attn(res_space)
        res_space = self.drop_path(res_space)
        x = x + res_space

        ## Mlp
        mlp = self.norm(x)
        mlp = self.mlp(mlp)
        mlp = self.drop_path(mlp)
        x = x + mlp
        return x


class PatchEmbed(nn.Module):
    """Split image into patches and then embed them.
    Parameters
    ----------
    patch_size : int
        Size of the patch (it is a square).
    in_chans : int
        Number of input channels.
    embed_dim : int
        The emmbedding dimension.
    Attributes
    ----------
    proj : nn.Conv2d
        Convolutional layer that does both the splitting into patches
        and their embedding.
    """

    def __init__(
        self,
        frames,
        patch_size:Tuple[int, int]=(16,16),
        in_chans=3,
        embed_dim=768,
    ):
        super().__init__()
        self.proj = nn.Conv2d(
            in_chans, embed_dim, kernel_size=patch_size, stride=patch_size
        )
        self.frames = frames

    def forward(self, x: Tensor) -> Tensor:
        """ Forward pass.
        Parameters
        ----------
        x : Tensor of shape (B, T, C, H, W)
        """
       
        x = rearrange(x, "b t ... -> (b t) ...")
        # (batch, time, embed_dim, n_patches ** 0.5, n_patches ** 0.5)
        x = self.proj(x)
        # (batch * time, n_patches, embed_dim)
        x = rearrange(x, "(b t) d h w -> b t (h w) d", t=self.frames)
        return x