from typing import Optional

import torch
from torch import nn, Tensor


class VidTr(nn.Module):
    r"""VidTR.
    Args:
        d_model (int): Number of channels in hidden layers.
        nhead (int): Number of attention heads.
        num_encoder_layers (int): Number of encoder layers
        dim_feedforward (int): Number of channels in MLP
        dropout (float): Attention dropout rate. Default: 0.1
        activatioon (str): Activation function
        normalize_before (bool): Layer norm before MLP
        patch_size (tuple): patch size
        in_channel (int): Number of input channels
        activity_num (int): Class number
        temporal_size (int): Number of frames in temporal
        merge_later (bool): Use joint spatio-temporal attention in later transformer layers
        merge_index (int): Start layer index that use joint spatio-temporal attention
    """

    def __init__(
        self,
        d_model=512,
        nhead=8,
        num_encoder_layers=6,
        dim_feedforward=2048,
        dropout=0.1,
        activation="relu",
        normalize_before=True,
        patch_size=(1, 16, 16),
        in_channel=3,
        activity_num=157,
        merge_index=6,
        temporal_size=16,
        merge_later=False,
    ):
        super().__init__()

        self.temporal_size = temporal_size
        self.merge_later = merge_later

        self.conv_stem = nn.Conv3d(
            in_channels=in_channel,
            out_channels=d_model,
            kernel_size=patch_size,
            stride=patch_size,
            bias=True,
        )
        pos_embedding_layer_wise = True if self.temporal_size == 16 else False
        layer_list = []
        for i in range(num_encoder_layers):
            module_temp = TransformerEncoderLayer(
                d_model,
                nhead,
                dim_feedforward,
                dropout,
                activation,
                normalize_before,
                merge_index=merge_index,
                merge_later=merge_later,
                layer_index=i,
                pos_embedding_layer_wise=pos_embedding_layer_wise,
            )
            layer_list.append(module_temp)
        encoder_layers = nn.ModuleList(layer_list)

        encoder_norm = nn.LayerNorm(d_model) if normalize_before else None
        self.encoder = TransformerEncoder(
            encoder_layers, num_encoder_layers, encoder_norm
        )

        self._reset_parameters()

        self.d_model = d_model
        self.nhead = nhead
        self.avg_pool = nn.AdaptiveAvgPool3d((1, 1, 1))
        self.fc = nn.Linear(in_features=d_model, out_features=activity_num, bias=True)

        if merge_later:
            self.cls = nn.Parameter(torch.Tensor(1, 1, d_model))
            self.pos_embedding = nn.Parameter(
                torch.Tensor(1, self.temporal_size * 14 * 14 + 1, self.d_model)
            )
        else:
            self.pos_embedding = nn.Parameter(
                torch.Tensor(1, (self.temporal_size + 1) * (14 * 14 + 1), self.d_model)
            )
            self.cls_s = nn.Parameter(torch.Tensor(temporal_size, 1, d_model))
            self.cls_t = nn.Parameter(torch.Tensor(1, 14 * 14 + 1, d_model))
        self.dropout = nn.Dropout(0.5)
        self.dp_pos = nn.Dropout(0.1)


    def _reset_parameters(self):
        for p in self.parameters():
            if p.dim() > 1:
                nn.init.xavier_uniform_(p)

    def forward(self, src):
        # flatten NxCxHxW to HWxNxC
        src = self.conv_stem(src)
        bs, c, t, h, w = src.shape

        pos_embed = self.pos_embedding.permute((1, 0, 2)).repeat(1, bs, 1)
        
        if self.merge_later:
            src = src.flatten(2).permute(2, 0, 1)
            cls = self.cls.repeat(1, bs, 1)
            src = torch.cat((cls, src), dim=0)
        else:
            src = src.flatten(3).permute(2, 3, 0, 1)
            cls_t = self.cls_t.view(1, w * h + 1, 1, self.d_model).repeat(1, 1, bs, 1)
            cls_s = self.cls_s.view(t, 1, 1, self.d_model).repeat(1, 1, bs, 1)

            src = torch.cat((cls_s, src), dim=1)
            src = torch.cat((cls_t, src), dim=0)
        src = src.view(-1, bs, self.d_model)

        memory = self.encoder(
            src, (bs, c, t, h, w), src_key_padding_mask=None, pos=pos_embed
        )

        out = (
            memory.view(t + 1, w * h + 1, bs, c)[0, 0, :, :]
            if not self.merge_later
            else memory[0, :, :]
        )
        out = self.dropout(out.view(bs, -1))
        out = self.fc(out)

        return out


class TransformerEncoder(nn.Module):

    r"""Transformer Encoder of Compact VidTR.
    Args:
        layers (int): Transformer layer
        num_layers (int): Number of transformer layers
        norm (LayerNorm): Layer Norm
    """

    def __init__(self, encoder_layers, num_layers, norm=None):
        super().__init__()
        self.layers = encoder_layers
        self.num_layers = num_layers
        self.norm = norm

    def forward(
        self,
        src,
        orig_shape,
        mask: Optional[Tensor] = None,
        src_key_padding_mask: Optional[Tensor] = None,
        pos: Optional[Tensor] = None,
    ):
        output = src

        for layer in self.layers:
            output, pos, orig_shape = layer(
                output,
                orig_shape,
                src_mask=mask,
                src_key_padding_mask=src_key_padding_mask,
                pos=pos,
            )

        if self.norm is not None:
            output = self.norm(output)

        return output


class TransformerEncoderLayer(nn.Module):

    r"""Transformer block of Compact VidTR.
    Args:
        d_model (int): Number of channels in hidden layers.
        nhead (int): Number of attention heads.
        dim_feedforward (int): Number of channels in MLP
        activatioon (str): Activation function
        normalize_before (bool): Layer norm before MLP
        dropout (float): Attention dropout rate.
        layer_index (int): Layer index of transformer layer.
        merge_later (bool): Use joint spatio-temporal attention in later transformer layers
        merge_index (int): Start layer index that use joint spatio-temporal attention
        pos_embedding_layer_wise (bool): Add position embedding on every layer
    """

    def __init__(
        self,
        d_model,
        nhead,
        dim_feedforward=2048,
        dropout=0.1,
        normalize_before=True,
        merge_index=6,
        merge_later=False,
        layer_index=0,
        pos_embedding_layer_wise=False,
    ):
        super().__init__()
        self.d_model = d_model
        self.nhead = nhead
        self.dropout_rate = dropout
        self.merge_later = merge_later
        self.merge_index = merge_index
        self.pos_embedding_layer_wise = pos_embedding_layer_wise

        if merge_later and layer_index > merge_index:
            self.self_attn = nn.MultiheadAttention(
                d_model, nhead, dropout=dropout, kdim=d_model, vdim=d_model
            )
        elif merge_later:
            self.self_attn = MultiHeadTemperalPoolAttentionNoToken(
                nhead, d_model, dropout=dropout
            )
        else:
            self.self_attn = MultiHeadSplitAttentionDualToken(
                nhead, d_model, dropout=dropout
            )
        # Implementation of Feedforward model
        self.linear1 = nn.Linear(d_model, dim_feedforward)
        self.dropout = nn.Dropout(dropout)
        self.linear2 = nn.Linear(dim_feedforward, d_model)

        self.norm1 = nn.LayerNorm(d_model)
        self.norm2 = nn.LayerNorm(d_model)
        self.dropout1 = nn.Dropout(dropout)
        self.dropout2 = nn.Dropout(dropout)

        self.activation = nn.GELU()
        self.normalize_before = normalize_before
        self.layer_index = layer_index

    def with_pos_embed(self, tensor, pos: Optional[Tensor]):
        return tensor if pos is None else tensor + pos

    def forward_pre(
        self,
        src,
        orig_shape,
        src_mask: Optional[Tensor] = None,
        src_key_padding_mask: Optional[Tensor] = None,
        pos: Optional[Tensor] = None,
    ):
        b, c, t, w, h = orig_shape
        src2 = self.norm1(src)
        if self.layer_index == 0 or self.pos_embedding_layer_wise:
            q = k = self.with_pos_embed(src2, pos)
            v = src2
        else:
            q = k = v = src2

        if self.merge_later and self.layer_index > self.merge_index:
            src_attn = self.self_attn(
                q, k, value=v, attn_mask=src_mask, key_padding_mask=src_key_padding_mask
            )
        else:
            src_attn = self.self_attn(
                q,
                k,
                value=v,
                orig_shape=orig_shape,
                attn_mask=src_mask,
                key_padding_mask=src_key_padding_mask,
            )
        src2 = src_attn[0]

        src = src + self.dropout1(src2)
        src2 = self.norm2(src)
        src2 = self.linear2(self.dropout(self.activation(self.linear1(src2))))
        src = src + self.dropout2(src2)
        return src, pos, orig_shape

    def forward(
        self,
        src,
        orig_shape,
        src_mask: Optional[Tensor] = None,
        src_key_padding_mask: Optional[Tensor] = None,
        pos: Optional[Tensor] = None,
    ):
        if self.normalize_before:
            return self.forward_pre(
                src, orig_shape, src_mask, src_key_padding_mask, pos
            )
        return self.forward_post(src, orig_shape, src_mask, src_key_padding_mask, pos)


class MultiHeadSplitAttentionDualToken(nn.Module):
    """Multi-Head Attention module"""

    def __init__(self, head_num, d_model, dropout=0.1):
        super().__init__()

        self.head_num = head_num
        self.d_model = d_model
        self.in_proj_weight = nn.Parameter(torch.Tensor(d_model * 3, d_model))
        self.in_proj_bias = nn.Parameter(
            torch.Tensor(
                d_model * 3,
            )
        )

        self.attention_t = ScaledDotProductAttention(attn_dropout=dropout)
        self.attention_s = ScaledDotProductAttention(attn_dropout=dropout)

        self.avg_pool = nn.AvgPool2d(kernel_size=(2, 1), stride=(2, 1))

        self.out_proj = nn.Linear(d_model, d_model)

        self.dropout = nn.Dropout(dropout)

    def forward(self, q, k, value, orig_shape, attn_mask=None, key_padding_mask=None):
        mask = attn_mask

        b, c, t, w, h = orig_shape

        seq_l, sz_b, c = q.shape

        qkv = q @ self.in_proj_weight.t() + self.in_proj_bias

        q = qkv[:, :, :c]
        k = qkv[:, :, c : 2 * c]
        v = qkv[:, :, 2 * c :]

        q_s = (
            q.view(t + 1, w * h + 1, b, self.head_num, c // self.head_num)
            .permute(3, 2, 4, 0, 1)
            .contiguous()
            .view(self.head_num * b, c // self.head_num, t + 1, w * h + 1)
        )
        k_s = (
            k.view(t + 1, w * h + 1, b, self.head_num, c // self.head_num)
            .permute(3, 2, 4, 0, 1)
            .contiguous()
            .view(self.head_num * b, c // self.head_num, t + 1, w * h + 1)
        )

        q_t = (
            q.view(t + 1, w * h + 1, b, self.head_num, c // self.head_num)
            .permute(3, 2, 1, 0, 4)
            .contiguous()
            .view(-1, t + 1, self.d_model // self.head_num)
        )
        k_t = (
            k.view(t + 1, w * h + 1, b, self.head_num, c // self.head_num)
            .permute(3, 2, 1, 0, 4)
            .contiguous()
            .view(-1, t + 1, self.d_model // self.head_num)
        )

        v_t = (
            v.view(t + 1, w * h + 1, b, self.head_num, c // self.head_num)
            .permute(3, 2, 1, 0, 4)
            .contiguous()
            .view(-1, t + 1, self.d_model // self.head_num)
        )

        output_t, attnx = self.attention_t(q_t, k_t, v_t, mask=mask)

        v_s = (
            output_t.view(
                self.head_num, b, w * h + 1, t + 1, self.d_model // self.head_num
            )
            .permute(0, 1, 3, 2, 4)
            .contiguous()
            .view(self.head_num * sz_b * (t + 1), w * h + 1, -1)
        )

        q_s = (
            q_s.permute(0, 2, 3, 1).contiguous().view(-1, w * h + 1, c // self.head_num)
        )
        k_s = (
            k_s.permute(0, 2, 3, 1).contiguous().view(-1, w * h + 1, c // self.head_num)
        )
        output_s, attn = self.attention_s(q_s, k_s, v_s, mask=mask)
        _, seq_l, _ = output_s.shape
        output = (
            output_s.view(
                self.head_num, b, -1, w * h + 1, self.d_model // self.head_num
            )
            .permute(2, 3, 1, 0, 4)
            .contiguous()
            .view(-1, b, c)
        )
        output = self.out_proj(output)

        return output, attn


class MultiHeadTemperalPoolAttentionNoToken(nn.Module):
    """Multi-Head Attention module"""

    def __init__(self, head_num, d_model, dropout):
        super().__init__()

        self.head_num = head_num
        self.d_model = d_model
        self.in_proj_weight = nn.Parameter(torch.Tensor(d_model * 3, d_model))
        self.in_proj_bias = nn.Parameter(
            torch.Tensor(
                d_model * 3,
            )
        )
        torch.zeros(128) + 0.04
        self.attention_t = ScaledDotProductAttention(attn_dropout=dropout)
        self.attention_s = ScaledDotProductAttention(attn_dropout=dropout)

        self.avg_pool = nn.AvgPool2d(kernel_size=(2, 1), stride=(2, 1))

        self.out_proj = nn.Linear(d_model, d_model)

    def forward(self, q, k, value, orig_shape, attn_mask=None, key_padding_mask=None):
        mask = attn_mask

        b, c, t, w, h = orig_shape

        seq_l, sz_b, c = q.shape

        q_cls = q[:1, :, :]
        q = q[1:, :, :]

        qkv = q @ self.in_proj_weight.t() + self.in_proj_bias

        q = qkv[:, :, :c]
        k = qkv[:, :, c : 2 * c]
        v = qkv[:, :, 2 * c :]

        q_s = (
            q.view(t, w * h, b, self.head_num, c // self.head_num)
            .permute(3, 2, 4, 0, 1)
            .contiguous()
            .view(self.head_num * b, c // self.head_num, t, w * h)
        )
        k_s = (
            k.view(t, w * h, b, self.head_num, c // self.head_num)
            .permute(3, 2, 4, 0, 1)
            .contiguous()
            .view(self.head_num * b, c // self.head_num, t, w * h)
        )

        q_t = (
            q.view(t, w * h, b, self.head_num, c // self.head_num)
            .permute(3, 2, 1, 0, 4)
            .contiguous()
            .view(-1, t, self.d_model // self.head_num)
        )
        k_t = (
            k.view(t, w * h, b, self.head_num, c // self.head_num)
            .permute(3, 2, 1, 0, 4)
            .contiguous()
            .view(-1, t, self.d_model // self.head_num)
        )

        v_t = (
            v.view(t, w * h, b, self.head_num, c // self.head_num)
            .permute(3, 2, 1, 0, 4)
            .contiguous()
            .view(-1, t, self.d_model // self.head_num)
        )

        output_t, attnx = self.attention_t(q_t, k_t, v_t, mask=mask)
        v_s = (
                output_t.view(self.head_num, b, w * h, t, self.d_model // self.head_num)
                .permute(0, 1, 3, 2, 4)
                .contiguous()
                .view(self.head_num * sz_b * t, w * h, -1)
            )

        q_s = q_s.permute(0, 2, 3, 1).contiguous().view(-1, w * h, c // self.head_num)
        k_s = k_s.permute(0, 2, 3, 1).contiguous().view(-1, w * h, c // self.head_num)
        output_s, attn = self.attention_s(q_s, k_s, v_s, mask=mask)
        _, seq_l, _ = output_s.shape
        output = (
            output_s.view(self.head_num, b, -1, w * h, self.d_model // self.head_num)
            .permute(2, 3, 1, 0, 4)
            .contiguous()
            .view(-1, b, c)
        )
        output = self.out_proj(output)
        output = torch.cat((q_cls, output), dim=0)

        return output, attn


class ScaledDotProductAttention(nn.Module):
    ''' Scaled Dot-Product Attention '''

    def __init__(self, attn_dropout=0.5):
        super().__init__()
        self.dropout = nn.Dropout(attn_dropout)
        self.softmax = nn.Softmax(dim=2)

    def forward(self, q, k, v, mask=None):
        temperature = k.shape[-1] ** 0.5
        attn = torch.bmm(q, k.permute(0, 2, 1))
        attn = attn / temperature

        attn = self.softmax(attn)
        if mask is not None:
            attn = attn + mask
        attn = self.dropout(attn)
        output = torch.bmm(attn, v)

        return output, attn